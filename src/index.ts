import * as R from 'ramda';
import jsonata from 'jsonata';
import moment from 'moment';

import data from '../data.js';

const result = jsonata(
  `(
    $start := '2021-02-14';
    $end := '2021-02-20';
    
    $workLists := $.data.googleTaskLists.{remoteId:title};
    $workTasks := $.data.tasks[google];
    
    $completedWithinRange := $workTasks[
        ($toMillis($start, '[Y]-[M]-[D]')<task.completionDate) and 
        (task.completionDate<$toMillis($end, '[Y]-[M]-[D]'))
      ];
    
    $workSubtasksGroupedByParentId := $completedWithinRange[google.remoteParent]{google.remoteParent: $};    
    $paretsWithCompletedSubtasks := $workTasks[google.remoteId in $completedWithinRange.google.remoteParent];
    $workRootTasks := $completedWithinRange[$not($exists(google.remoteParent))];
    $distinct($append($paretsWithCompletedSubtasks, $workRootTasks))
      {
        $lookup($workLists, $.google.listId): 
          [$.{
            "task": $.task, 
            "subtasks": [$lookup($workSubtasksGroupedByParentId, google.remoteId).task]
          }]
      };
    )`
).evaluate(data);

// console.log(JSON.stringify(result, null, 2));

const printDuration = seconds =>
  `**[${moment.duration(seconds, 'seconds').humanize()}]**`;

const printSubtask = task =>
  `\n  - [${task.completionDate ? 'x' : ' '}] ${task.title}.${printDuration(
    task.estimatedSeconds
  )}`;

const printTask = ({ task, subtasks }) => {
  const subtasksDuration = R.sum(subtasks.map(R.prop('estimatedSeconds')));
  return `- [${task.completionDate ? 'x' : ' '}] ${task.title}.${printDuration(
    task.estimatedSeconds + subtasksDuration
  )}${subtasks.map(printSubtask).join('')}`;
};
const printList = (val, key) =>
  console.log(`### ${key}\n\n${val.map(printTask).join('\n')}\n`);

R.forEachObjIndexed(printList, result);
